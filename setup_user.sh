#!/bin/bash

# Set variables
DEBUG=false
CLEAN=true

print_message() {
  local message="$1"
  echo "------------------------------------------------------------------"
  echo "   $message"
  echo "------------------------------------------------------------------"
}

if [ "$DEBUG" = true ]; then
    print_message "DEBUG mode is enabled. Skipping some steps!"
else
    # copy bash_aliases
    print_message "Copying bash aliases"
    cp ./files/bash/bash_aliases ~/.bash_aliases    

    # copy custom bashrc
    print_message "Copying bashrc"
    rm ~/.bashrc
    cp ./files/bash/user.bashrc ~/.bashrc
    chmod 644 ~/.bashrc
    source ~/.bashrc
    
    # setup conky
    print_message "Copying conky"
    mkdir ~/.fonts
    cp ./files/conky/conkyrc ~/.conkyrc 
    cp ./files/conky/Poky.ttf ~/.fonts/Poky.ttf
    
    # setup xfce4
    print_message "Setting up xfce4"
    rm -rf ~/.config/xfce4
    cp -r ./files/xfce4 ~/.config/
    chown -hR $USER:$USER ~/.config/xfce4
    
    # setup libreoffice
    print_message "Setting up libreoffice"
    rm -rf ~/.config/libreoffice
    cp -r ./files/libreoffice ~/.config/
    chown -hR $USER:$USER ~/.config/libreoffice
    
    # setup obsidian
    print_message "Setting up obsidian"
    mkdir ~/Dokumenty/
    rm -rf ~/Dokumenty/obsidian_vault
    cp -r ./files/obsidian_vault ~/Dokumenty/
    chown -hR $USER:$USER ~/Dokumenty/obsidian_vault
    
    # install git prompt
    print_message "Installing git prompt"
    git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1
    cat >> ~/.bashrc<< EOF
#--------------------git-prompt----------------------
if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    source "$HOME/.bash-git-prompt/gitprompt.sh"
fi
EOF

    #########################################################
    # install kubernetes power tools
    print_message "Installing kubernetes power tools"

    # bash complete alias
    mkdir ~/.bash_completion.d
    curl https://raw.githubusercontent.com/cykerway/complete-alias/master/complete_alias >> ~/.bash_completion.d/complete_alias
    
    # bash kube aliases
    wget -O ~/.bash_kube_aliases https://gitlab.com/janjansmid/kubernetes-power-tools/-/raw/main/bash_kube_aliases

    # .kube directory
    mkdir ~/.kube
    # touch ~/.kube/config
    # chmod 700 ~/.kube/config
    
    # kube ps1 prompt
    curl https://raw.githubusercontent.com/jonmosco/kube-ps1/master/kube-ps1.sh >> ~/.kube/kube-ps1.sh

    # Tweak kube-ps1.sh to work with kcx
    sed -i 's/stat -L -c/stat -c/g' ~/.kube/kube-ps1.sh
    sed -i 's/KUBE_PS1_LAST_TIME=$(/KUBE_PS1_LAST_TIME=0 #$(/g' ~/.kube/kube-ps1.sh
    #########################################################

fi

print_message "Finished!"

