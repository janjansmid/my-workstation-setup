#!/bin/bash

# Set variables
DEBUG=false
CLEAN=true

print_message() {
  local message="$1"
  echo "------------------------------------------------------------------"
  echo "   $message"
  echo "------------------------------------------------------------------"
}

if [ "$DEBUG" = true ]; then
    print_message "DEBUG mode is enabled. Skipping some steps!"
else
    # Prepare repository list
    print_message "Preparing sources.list."
    rm /etc/apt/sources.list
    cp ./files/sources.list /etc/apt/sources.list

    # Update the package list
    print_message "Upgrading installed packages"
    apt update
    apt upgrade -y

    # Install basic packages
    print_message "Installing basic packages"
    apt install -y chromium firefox-esr ca-certificates 
    apt install -y nala aptitude unattended-upgrades apt-transport-https
    apt install -y gvfs-backends gparted gsmartcontrol gnome-disk-utility
    apt install -y gigolo virt-viewer remmina remmina-plugin-rdp remmina-plugin-vnc
    apt install -y git diffuse curl wget less geany micro nano vim meld ncdu xpad tmux jq
    apt install -y mc htop btop iftop bash-completion conky-all nmap x11-utils nload iperf3
    apt install -y network-manager-openvpn network-manager-openvpn-gnome wireguard wireguard-tools syslog-ng
    apt install -y vlc gthumb flameshot gimp seahorse lightdm-settings slick-greeter gnome-calculator file-roller mlocate
    apt install -y shiki-dust-theme baobab bleachbit onboard xfce4-mount-plugin orage libreoffice-l10n-cs libreoffice-style-breeze
    apt remove  -y xarchiver

    # install dbeaver
    print_message "Installing dbeaver"
    wget -O dbeaver.deb https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb
    dpkg -i dbeaver.deb

    # install obsidian
    print_message "Installing Obsidian"
    wget -O obsidian.deb https://github.com/obsidianmd/obsidian-releases/releases/download/v1.4.16/obsidian_1.4.16_amd64.deb
    dpkg -i obsidian.deb

    # install vscode
    print_message "Installing VS Code"
    wget wget -O code.deb https://go.microsoft.com/fwlink/?LinkID=760868
    dpkg -i code.deb

    # install mailspring
    print_message "Installing Mailspring"
    wget -O mailspring.deb https://updates.getmailspring.com/download?platform=linuxDeb
    apt install libsecret-1-dev
    dpkg -i mailspring.deb

    # copy custom bashrc
    print_message "Setting up bashrc"
    rm /etc/bash.bashrc
    rm /root/.bashrc
    cp ./files/bash/bash.bashrc /etc/bash.bashrc
    cp ./files/bash/root.bashrc /root/.bashrc
    chmod 644 /etc/bash.bashrc
    chmod 644 /root/.bashrc

    # download some backgrounds
    print_message "Downloading background"
    mkdir /usr/share/backgrounds/space/
    wget -O /usr/share/backgrounds/space/mars.png https://w.wallhaven.cc/full/r2/wallhaven-r2wvk7.png 
    wget -O /usr/share/backgrounds/space/unone.png https://w.wallhaven.cc/full/ym/wallhaven-ymwyzg.png
    
    # copy custom lightdm
    print_message "Setting up lightdm"
    rm /etc/lightdm/lightdm.conf
    cp ./files/lightdm/lightdm.conf /etc/lightdm/lightdm.conf
    
    # copy custom keyboard config
    print_message "Setting up console keymap"
    rm /etc/default/keyboard
    cp ./files/keyboard /etc/default/keyboard
    
    # set console keymap
    # echo "KEYMAP=cz" >> /etc/vconsole.conf 

    ###################################################################
    ## install kubernetes tooling
    print_message "Instaling kubernetes tooling"
    rm /etc/apt/sources.list.d/kubernetes.list
    rm /etc/apt/sources.list.d/helm-stable-debian.list    
    
    ## add k8s repository
    curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
    
    ## add helm repository
    curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    
    ## install packages
    sudo apt update
    sudo apt install -y helm kubectl kubectx fzf

    ## install k9s
    wget -O k9s_linux_amd64.deb https://github.com/derailed/k9s/releases/download/v0.31.5/k9s_linux_amd64.deb
    dpkg -i k9s_linux_amd64.deb
    ###################################################################

    # reconfigure unattended upgrades
    dpkg-reconfigure unattended-upgrades
fi

if [ "$CLEAN" = true ]; then
    print_message "Cleaning up downloaded packages"
    # todo : when interactive , query user "do you wanna clenaup"?
    # cleanup
    apt clean all
    rm *.deb
fi

print_message "Finished!"