# my workstation setup
Why? Because I dont want to set up my computer all over again manually in case I can not simply clone my prepared and encrypted system image.
Based on Debian 12 with Xfce Desktop Environment.


## How to use:
```
sudo apt update && sudo apt install git
git clone https://gitlab.com/janjansmid/my-workstation-setup.git
cd my-workstation-setup
sudo bash setup_system.sh
bash setup_user.sh
sudo reboot
```

![My Desktop](desktop.png)

